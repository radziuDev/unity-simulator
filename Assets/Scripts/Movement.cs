using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Movement : MonoBehaviour {
  private NavMeshAgent agent;
  private Animator animator;
  private Surrounding surrounding;
  private Status status;

  [SerializeField]
  private AnimationCurve jumpCurve = new AnimationCurve();


  [HideInInspector]
  public bool isInQueue = false;

  private void Start() {
    agent = GetComponent<NavMeshAgent>();
    animator = GetComponent<Animator>();
    surrounding = GetComponent<Surrounding>();
    status = GetComponent<Status>();

    StartCoroutine(HandleJumps());
  }

  private void Update() {
    animator.SetBool("isWalking", false);
    animator.SetBool("isJumping", false);
    agent.isStopped = true;
    
    if (isInQueue) return;

    if (!status.destination) {
      LayerMask mask = LayerMask.GetMask("Hallway");
      Collider collider = surrounding.CheckForLayer(mask, true);

      if (collider && Vector3.Distance(transform.position, collider.transform.position) >= 4f)
        HandleMovement(collider.transform.position);
      
      return;
    }

    if (Vector3.Distance(transform.position, status.destination.transform.position) > 3.5f) {
      HandleMovement(status.destination.transform.position);
      return;  
    }

    float distance = Vector3.Distance(
      new Vector3(transform.position.x, 0, transform.position.z),
      new Vector3(status.destination.transform.position.x, 0, status.destination.transform.position.z)
    );

    if (distance >= 1f) HandleMovement(status.destination.transform.position);
  }

  private void HandleMovement(Vector3 position) {
    animator.SetBool("isWalking", true);
    agent.isStopped = false;

    agent.SetDestination(position);
  }

  IEnumerator HandleJumps() {
    agent.autoTraverseOffMeshLink = false;
    
    while (true) {
      if (agent.isOnOffMeshLink) {
        yield return StartCoroutine(Curve(agent, 0.5f));
        agent.CompleteOffMeshLink();
      }
      yield return null;
    }
  }

  // Stolen from here https://github.com/Unity-Technologies/NavMeshComponents/blob/master/Assets/Examples/Scripts/AgentLinkMover.cs
  IEnumerator Curve(NavMeshAgent agent, float duration) {
    agent.isStopped = false;
    animator.SetBool("isJumping", true);

    OffMeshLinkData data = agent.currentOffMeshLinkData;
    Vector3 startPos = agent.transform.position;
    Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
    float normalizedTime = 0.0f;

    while (normalizedTime < 1.0f) {
      float yOffset = jumpCurve.Evaluate(normalizedTime);
      agent.transform.position = Vector3.Lerp(startPos, endPos, normalizedTime) + yOffset * Vector3.up;
      normalizedTime += Time.deltaTime / duration;
      yield return null;
    }
  }
}
