using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatisfyTheNeed : MonoBehaviour {
  [HideInInspector]
  public List<GameObject> queue = new List<GameObject>();
  public List<GameObject> queuePlaces = new List<GameObject>();

  [SerializeField]
  private float taskDuration = 2f;

  private Dictionary<string, string> animationByLayer = new Dictionary<string, string>() {
    { "Hydration", "isDrinking" },
    { "Sleepiness", "isSleeping" },
    { "Satiety", "isEating" },
    { "Entertainment", "isWatching" }
  };

  private void Update() {
    foreach (GameObject obj in queue) {
      GameObject place = GetMyPlace(obj);

      float distance = Vector3.Distance(
        new Vector3(obj.transform.position.x, 0, obj.transform.position.z),
        new Vector3(place.transform.position.x, 0, place.transform.position.z)
      );

      if (distance > 1f) continue;

      Vector3 direction =  obj.transform.position - transform.position;
      Quaternion targetRotation = Quaternion.LookRotation(-direction, Vector3.up);
      Quaternion targetRotationAxisY = Quaternion.Euler(0f, targetRotation.eulerAngles.y, 0f);

      obj.transform.rotation = Quaternion.Slerp(obj.transform.rotation, targetRotationAxisY, 15f * Time.deltaTime);
    }
  }

  public void JoinToQueue(GameObject self) {
    queue.Add(self);
  }

  public GameObject GetMyPlace(GameObject self) {
    if (!queue.Contains(self)) return null;

    int index = queue.FindIndex(obj => obj == self);
    if (index == -1) return null;

    return queuePlaces[index];
  }

  public int GetMyQueueIndex (GameObject self) => queue.FindIndex(obj => obj == self);

  public IEnumerator Satisfy(GameObject self) {
    Animator animator = self.GetComponent<Animator>();
    Status status = self.GetComponent<Status>();

    string layer = LayerMask.LayerToName(gameObject.layer);
    animator.SetBool(animationByLayer[layer], true);

    float elapsedTime = 0f;

    while (elapsedTime < taskDuration) {
      elapsedTime += Time.deltaTime;
      yield return null;
    }
    
    if (layer == "Hydration") status.hydration = status.maxHydration;
    if (layer == "Sleepiness") status.sleepiness = status.maxSleepiness;
    if (layer == "Satiety") status.satiety = status.maxSatiety;
    if (layer == "Entertainment") status.entertainment = status.maxEntertainment;
    animator.SetBool(animationByLayer[layer], false);
    
    queue.Remove(self);
    status.EndTask();
  }
}
