using UnityEngine;
using System.Collections.Generic;

public class PathBuss : MonoBehaviour {
  private List<Collider> previousQueue = new List<Collider>();
  private List<Collider> queue = new List<Collider>();
  private int iteration = 0;

  [SerializeField]
  private int simultaneouslyNumber = 2;

  private void Update() {
    int index = 0;

    foreach(Collider colision in queue) {
      Movement movementScript = colision.gameObject.GetComponent<Movement>();

      if (index <= simultaneouslyNumber - 1) {
        index += 1;
        movementScript.isInQueue = false;
        continue;
      }

      movementScript.isInQueue = true;
      index += 1;
    }
  }

  private void LateUpdate() {
    previousQueue = queue;
    iteration = 0;
  }

  private void OnTriggerStay(Collider collision) {
    if (collision.gameObject.layer != 3) return;

    if (iteration == 0) queue = new List<Collider>();
    iteration += 1;

    if (!queue.Contains(collision)) queue.Add(collision);

    List<Collider> sortedList = new List<Collider>();
    foreach (Collider item in previousQueue) if (queue.Contains(item)) sortedList.Add(item);
    foreach (Collider item in queue) if (!sortedList.Contains(item)) sortedList.Add(item);
    
    queue = sortedList;
  }

  private void OnTriggerExit(Collider collision) {
    if (collision.gameObject.layer != 3) return;
    Movement movementScript = collision.gameObject.GetComponent<Movement>();
    movementScript.isInQueue = false;
  }
}
