using System.Collections.Generic;
using UnityEngine;

public class Light : MonoBehaviour {
  [SerializeField]
  private GameObject bulb;

  private List<Collider> persons = new List<Collider>();
  
  void Start() {
    bulb.SetActive(false);
  }

  private void LateUpdate() {
    bulb.SetActive(persons.Count > 0);
  }

  private void OnTriggerStay(Collider collision) {
    if (
      collision.gameObject.layer == 3
      && !persons.Contains(collision)
    ) persons.Add(collision);
  }

  private void OnTriggerExit(Collider collision) {
    if (
      collision.gameObject.layer == 3
      && persons.Contains(collision)
    ) persons.Remove(collision);
  }
}
