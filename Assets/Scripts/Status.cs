using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Status : MonoBehaviour {
  private Surrounding surrounding;

  [HideInInspector]
  public GameObject target;
  [HideInInspector]
  public GameObject destination;

  [HideInInspector]
  public bool isBussy = false;

  [HideInInspector]
  public float maxHydration;
  public float hydration;

  [HideInInspector]
  public float maxSleepiness;
  public float sleepiness;
  
  [HideInInspector]
  public float maxSatiety;
  public float satiety;
  
  [HideInInspector]
  public float maxEntertainment;
  public float entertainment;

  private List<int> tasks = new List<int>();
  private Coroutine taskCoroutine;
  
  private void Start() {
    surrounding = GetComponent<Surrounding>();

    maxHydration = Random.Range(60f, 150f);
    hydration = Random.Range(33f, maxHydration);

    maxSleepiness = Random.Range(60f, 150f);
    sleepiness = Random.Range(33f, maxSleepiness);
    
    maxSatiety = Random.Range(60f, 150f);
    satiety = Random.Range(33f, maxSatiety);
    
    maxEntertainment = Random.Range(60f, 150f);
    entertainment = Random.Range(33f, maxEntertainment);

    StartCoroutine(StatusRoutine());
    StartCoroutine(TaskRoutine());
  }

  private void Update() {
    if (!target || isBussy) return;

    SatisfyTheNeed obj = target.GetComponent<SatisfyTheNeed>();
    destination = obj.GetMyPlace(gameObject);

    if (Vector3.Distance(transform.position, destination.transform.position) > 3.5f) return;

    float relativeDistance = Vector3.Distance(
      new Vector3(transform.position.x, 0, transform.position.z),
      new Vector3(destination.transform.position.x, 0, destination.transform.position.z)
    );

    if (relativeDistance < 1f && obj.GetMyQueueIndex(gameObject) == 0) {
      taskCoroutine = StartCoroutine(obj.Satisfy(gameObject));
      target = null;
      isBussy = true;
    }
  }

  public void EndTask() {
    StopCoroutine(taskCoroutine);
    isBussy = false;
    destination = null;
  }

  private IEnumerator StatusRoutine() {
    WaitForSeconds wait = new WaitForSeconds(0.5f);

    while (true) {
      if(hydration > 0) hydration -= 1;
      if(sleepiness > 0) sleepiness -= 1;
      if(satiety > 0) satiety -= 1;
      if(entertainment > 0) entertainment -= 1;

      float[] values = {
        hydration,
        sleepiness,
        satiety,
        entertainment
      };

      for (int i = 0; i < values.Length; i++) {
        if (values[i] >= 33f && tasks.Contains(i))
          tasks.Remove(i); 

        if (values[i] < 33f && !tasks.Contains(i))
          tasks.Add(i);
      }
      
      yield return wait;
    }
  }

  private IEnumerator TaskRoutine() {
    WaitForSeconds wait = new WaitForSeconds(0.5f);

    while (true) {
      if (!target && tasks.Count > 0 && !isBussy) {
        List<string> layerNames = new List<string>() {
          "Hydration",
          "Sleepiness",
          "Satiety",
          "Entertainment"
        };

        LayerMask mask = LayerMask.GetMask(layerNames[tasks[0]]);
        Collider collider = surrounding.CheckForLayer(mask);

        if (collider) {
          target = collider.gameObject;
          target.GetComponent<SatisfyTheNeed>().JoinToQueue(gameObject);
        } else {
          int current = tasks[0];
          tasks.Remove(tasks[0]); 
          tasks.Add(current); 
        }
      }

      yield return wait;
    }
  }
}
