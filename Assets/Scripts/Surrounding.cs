using UnityEngine;

public class Surrounding : MonoBehaviour {
  public float radius = 50f;

  public Collider CheckForLayer(LayerMask layer, bool force = false) {
    Collider[] colliders = Physics.OverlapSphere(transform.position, radius, layer);

    if (force) {
      if (colliders.Length > 0) {
        Collider nearestColl = null;
        float nearestDistance = Mathf.Infinity;

        foreach (Collider collider in colliders) {
          float distance = Vector3.Distance(transform.position, collider.transform.position);
          if (distance < nearestDistance) {
            nearestDistance = distance;
            nearestColl = collider;
          }
        }

        return nearestColl;
      }
      return null;
    }

    Collider colliderWithLowestCount = null;
    int lowestCount = int.MaxValue;
    
    foreach (Collider collider in colliders) {
      SatisfyTheNeed satisfyComponent = collider.GetComponent<SatisfyTheNeed>();
      
      if (satisfyComponent != null && satisfyComponent.queue.Count < lowestCount) {
        lowestCount = satisfyComponent.queue.Count;
        colliderWithLowestCount = collider;
      }
    }

    if (!colliderWithLowestCount) return null;

    Collider nearestCollider = FindNearestCollider(colliders, lowestCount);
    SatisfyTheNeed script = nearestCollider.GetComponent<SatisfyTheNeed>(); 
    if (script.queuePlaces.Count <= script.queue.Count) return null;

    return nearestCollider;
  }

  private Collider FindNearestCollider(Collider[] colliders, int lowestCount) {
    Collider nearestCollider = null;
    float nearestDistance = Mathf.Infinity;

    foreach (Collider collider in colliders) {
      SatisfyTheNeed satisfyComponent = collider.GetComponent<SatisfyTheNeed>();
      if (satisfyComponent != null && satisfyComponent.queue.Count == lowestCount) {
        float distance = Vector3.Distance(gameObject.transform.position, collider.transform.position);
            if (distance < nearestDistance) {
              nearestDistance = distance;
              nearestCollider = collider;
            }
        }
    }

    return nearestCollider;
  }
}
