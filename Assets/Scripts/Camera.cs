using UnityEngine;
using Cinemachine;

public class Camera : MonoBehaviour {
  [SerializeField]
  private CinemachineFreeLook freeLookCamera;

  private int currentPlayer = 0;
  private Collider[] players;
  private bool isLoaded = false;

  private void Start() {
    UnityEngine.Cursor.lockState = CursorLockMode.Locked;
    UnityEngine.Cursor.visible = false;

    freeLookCamera = GameObject.FindObjectOfType<CinemachineFreeLook>();
  }

  private void Update() {
    if (!isLoaded) {
      LayerMask layer = LayerMask.GetMask("Player");
      players = Physics.OverlapSphere(transform.position, 200f, layer);
      isLoaded = true;
    }

    if (Input.GetMouseButtonDown(0)) {
      currentPlayer += 1;
      
      if (currentPlayer > players.Length - 1)
        currentPlayer = 0;
      
      swapCameraFollow();
    };

    if (Input.GetMouseButtonDown(1)) {
      currentPlayer -= 1;

      if (currentPlayer < 0)
        currentPlayer = players.Length - 1;
      
      swapCameraFollow();
    };
  }

  private void swapCameraFollow() {
    freeLookCamera.LookAt = players[currentPlayer].transform;
    freeLookCamera.Follow = players[currentPlayer].transform;
  }
}
