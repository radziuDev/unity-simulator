using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Surrounding))]
public class SurroundingTool : Editor {
  private void OnSceneGUI() {
    Surrounding surrounding = (Surrounding)target;

    Handles.color = Color.white;
    Handles.DrawWireArc(surrounding.transform.position, Vector3.up, Vector3.forward, 360, surrounding.radius);
  }
}
